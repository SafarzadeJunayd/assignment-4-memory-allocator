#define _DEFAULT_SOURCE

#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header *b, const char *fmt, ...);

void debug(const char *fmt, ...);

extern inline block_size size_from_capacity(block_capacity cap);

extern inline block_capacity capacity_from_size(block_size sz);

static bool block_is_big_enough(size_t query, struct block_header *block) {
    return block->capacity.bytes >= query;
}

static size_t pages_count(size_t mem) {
    return mem / getpagesize() + ((mem % getpagesize()) > 0);
}

static size_t round_pages(size_t mem) {
    return getpagesize() * pages_count(mem);
}

static void block_init(void *restrict addr, block_size block_sz, void *restrict next) {
    *((struct block_header *) addr) = (struct block_header) {
            .next = next,
            .capacity = capacity_from_size(block_sz),
            .is_free = true
    };
}

static size_t region_actual_size(size_t query) {
    return size_max(round_pages(query), REGION_MIN_SIZE);
}

extern inline bool region_is_invalid(const struct region *r);


static void *map_pages(void const *addr, size_t length, int additional_flags) {
    return mmap((void *) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags, -1, 0);
}

static struct region alloc_region(void const *addr, size_t query) {
    size_t region_size = region_actual_size(size_from_capacity((block_capacity) {query}).bytes);
    void *region_addr = map_pages(addr, region_size, MAP_FIXED_NOREPLACE);
    if (region_addr == MAP_FAILED) {
        region_addr = map_pages(addr, region_size, 0);
        if (region_addr == MAP_FAILED) return REGION_INVALID;
    }
    block_init(region_addr, (block_size) {region_size}, NULL);
    return (struct region) {.addr = region_addr, .size = region_size, .extends = region_addr == addr};
}

void *heap_init(size_t initial_size) {
    const struct region region = alloc_region(HEAP_START, initial_size);
    return region_is_invalid(&region) ? NULL : region.addr;
}


#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable(struct block_header *restrict block, size_t query) {
    return block->is_free &&
           query + offsetof(struct block_header, contents) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big(struct block_header *block, size_t query) {
    query = size_max(query, BLOCK_MIN_CAPACITY);
    if (!block || !block_splittable(block, query)) 
        return false;
    struct block_header *first_block = block;
    block_size first_block_size = size_from_capacity((block_capacity) {query});
    struct block_header *second_block = (void *)first_block + first_block_size.bytes;
    block_size second_block_size = {size_from_capacity(block->capacity).bytes - first_block_size.bytes};
    block_init(second_block, second_block_size, block->next);
    block_init(first_block, first_block_size, second_block);
    return true;
}

static void *block_after(struct block_header const *block) {
    return (void *) (block->contents + block->capacity.bytes);
}

static bool blocks_continuous(struct block_header const *fst, struct block_header const *snd) {
    return (void *) snd == block_after(fst);
}

static bool mergeable(struct block_header const *restrict fst, struct block_header const *restrict snd) {
    return fst->is_free && snd->is_free && blocks_continuous(fst, snd);
}

static bool try_merge_with_next(struct block_header *block) {
    if (!block || !block->next || !mergeable(block, block->next))
        return false;
    block->capacity.bytes += size_from_capacity(block->next->capacity).bytes;
    block->next = block->next->next;
    return true;
}

struct block_search_result {
    enum {
        BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED
    } type;
    struct block_header *block;
};

static struct block_search_result new_block_search_result(int type, struct block_header *block) {
    return (struct block_search_result) {type, block};
}

static struct block_search_result find_good_or_last(struct block_header *restrict block, size_t query) {
    if (!block) 
        return new_block_search_result(BSR_CORRUPTED, NULL);
    struct block_header *last = NULL;
    while (block) {
        while (block->next && try_merge_with_next(block)){}
        if (block->is_free && block_is_big_enough(query, block))
            return new_block_search_result(BSR_FOUND_GOOD_BLOCK, block);
        last = block;
        block = block->next;
    }
    return new_block_search_result(BSR_REACHED_END_NOT_FOUND, last);
}


static struct block_search_result try_memalloc_existing(size_t query, struct block_header *block) {
    query = size_max(query, BLOCK_MIN_CAPACITY);
    struct block_search_result search_result = find_good_or_last(block, query);
    if (search_result.type == BSR_FOUND_GOOD_BLOCK) {
        split_if_too_big(search_result.block, query);
        search_result.block->is_free = false;
    }
    return search_result;
}


static struct block_header *grow_heap(struct block_header *restrict last, size_t query) {
    if (!last) 
        return NULL;
    struct region new_region = alloc_region(block_after(last), size_max(query, BLOCK_MIN_CAPACITY));
    if (region_is_invalid(&new_region)) 
        return NULL;
    block_init(new_region.addr, (block_size) {new_region.size}, NULL);
    last->next = new_region.addr;
    if(last->is_free && try_merge_with_next(last))
        return last;
    return last->next;
}

static struct block_header *memalloc(size_t query, struct block_header *heap_start) {
    if (!heap_start) 
        return NULL;
    query = size_max(query, BLOCK_MIN_CAPACITY);
    struct block_search_result search_result = try_memalloc_existing(query, heap_start);
    if (search_result.type == BSR_FOUND_GOOD_BLOCK) 
        return search_result.block;
    if (search_result.type == BSR_CORRUPTED) 
        return NULL;
    struct block_header *block = grow_heap(search_result.block, query);
    if (!block) 
        return NULL;
    search_result = try_memalloc_existing(query, block);
    if(search_result.type == BSR_FOUND_GOOD_BLOCK) {
        return search_result.block;
    }
    return NULL;
}

void *_malloc(size_t query) {
    struct block_header *const addr = memalloc(query, (struct block_header *) HEAP_START);
    return addr ? addr->contents : NULL;
}

static struct block_header *block_get_header(void *contents) {
    return (struct block_header *) (((uint8_t *) contents) - offsetof(struct block_header, contents));
}

void _free(void *mem) {
    if (!mem) return;
    struct block_header *block = block_get_header(mem);
    block->is_free = true;
    while (try_merge_with_next(block)) {}
}

void heap_term() {
    struct block_header *block = HEAP_START;
    struct block_header *region_first_block = block;
    size_t region_size = 0;
    while (block) {
        region_size += size_from_capacity(block->capacity).bytes;
        struct block_header *next_block = block->next;
        if (!next_block || !blocks_continuous(block, next_block)) {
            munmap(region_first_block, region_size);
            region_first_block = next_block;
            region_size = 0;
        }
        block = next_block;
    }
}
