#include <assert.h>
#include <stdio.h>

#include "mem.h"
#include "mem_internals.h"

#define MAP_ANONYMOUS 0x20
#define BYTE_SIZE sizeof(uint8_t)

static struct block_header *get_block(void *mem) {
    return (struct block_header *) (mem - offsetof(struct block_header, contents));
}

void test_allocation() {
    printf("TEST 1: ALLOCATION\n");

    void *heap = heap_init(0);
    debug_heap(stdout, heap);

    void *block = _malloc(0);
    assert(block != NULL && "ALLOCATION TEST FAILED");

    debug_heap(stdout, heap);
    heap_term();
    printf("ALLOCATION TEST PASSED\n");
}

void test_free_one_block() {
    printf("TEST 2: FREE ONE BLOCK\n");

    void *heap = heap_init(0);
    debug_heap(stdout, heap);

    void *mem = _malloc(BYTE_SIZE);
    assert(mem != NULL);
    _free(mem);
    assert(get_block(mem)->is_free && "FREE ONE BLOCK TEST FAILED");

    debug_heap(stdout, heap);
    heap_term();
    printf("FREE ONE BLOCK TEST PASSED\n");
}

void test_free_two_blocks() {
    printf("TEST 2: FREE TWO BLOCKS\n");

    void *heap = heap_init(0);
    debug_heap(stdout, heap);

    void *first_block = _malloc(400);
    void *second_block = _malloc(BYTE_SIZE * 9);
    assert(first_block != NULL && second_block != NULL);
    _free(first_block);
    _free(second_block);
    assert(get_block(first_block)->is_free && get_block(second_block)->is_free && "FREE TWO BLOCKS TEST FAILED");

    debug_heap(stdout, heap);
    heap_term();
    printf("FREE ONE BLOCK TEST PASSED\n");
}

void test_region_extension() {
    printf("TEST 3: REGION EXTENSION\n");
    void *heap = heap_init(0);
    debug_heap(stdout, heap);

    void *first_block = _malloc(BYTE_SIZE * 512);
    void *second_block = _malloc(BYTE_SIZE * 512);
    assert(first_block != NULL && second_block != NULL);
    assert((void *)get_block(first_block) + offsetof(struct block_header, contents) + BYTE_SIZE * 512 == (void*)get_block(second_block) &&
            "REGION EXTENSION FAILED");

    debug_heap(stdout, heap);
    heap_term();
    printf("REGION EXTENSION TEST PASSED\n");
}

void test_region_creation() {
    printf("TEST 3: REGION CREATION\n");
    void *heap = heap_init(0);
    debug_heap(stdout, heap);

    void *mmap_addr = mmap(heap + REGION_MIN_SIZE,
                           REGION_MIN_SIZE,
                           PROT_READ | PROT_WRITE,
                           MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED,
                           -1, 0);

    assert(mmap_addr != MAP_FAILED);
    void *first_block = _malloc(BYTE_SIZE);
    void *second_block = _malloc(BYTE_SIZE * 14);
    assert(first_block && second_block);
    assert(get_block(second_block) != heap + REGION_MIN_SIZE && "REGION CREATION TEST FAILED");

    debug_heap(stdout, heap);
    heap_term();
    printf("REGION CREATION TEST PASSED\n");
}

int main() {
    test_allocation();
    test_free_one_block();
    test_free_two_blocks();
    test_region_extension();
    test_region_creation();
    return 0;
}
